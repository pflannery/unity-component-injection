# Component resolver library for Unity

Simplified way of resolving components automatically for a MonoBehaviour script.

> The unity scripting runtime must be using ".NET 4.x Equivalent"

```cs
public class Example : MonoBehaviour
{

    // You can also specify Inject(IncludeInActive = true) for From.Parent and From.Children.

    [Inject] BoxCollider boxCollider;

    [Inject(From.Parent)] Transform parentTransform;

    [Inject(From.Children)] Transform[] childTransforms;

    [Inject(From.Type)] Camera cam;

    // Will automatically detect array properties and use the respective
    // GetComponent/GetType methods to resolve them

    void Awake() => ResolveComponents(this);

    void Start()
    {
        Debug.Log(boxCollider);
        Debug.Log(childTransforms);
        Debug.Log(parentTransform);
        Debug.Log(cam);
    }

}

// You can also resolve components to a custom Class object 
// so long as it contains the Inject attributes.
public class SomeController : MonoBehaviour
{

    [Inject] Animator animator;

    void Awake() => ComponentResolver.ResolveComponents(this);

    void Start() => ComponentResolver.ResolveComponents(this, animator.GetBehaviour<SomeBehaviour>());
    
}

public class SomeBehaviour : StateMachineBehaviour
{

    [Inject] ShipController npc;
    [Inject] ShipComponent ship;

    override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        // your logic
    }

}

```
