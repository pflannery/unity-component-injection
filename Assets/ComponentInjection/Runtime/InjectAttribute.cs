﻿using System;

namespace ComponentInjection
{
    public enum From
    {
        Self = 0,
        Parent = 2,
        Children = 4,
        Type = 6
    }

    [AttributeUsage(AttributeTargets.Field | AttributeTargets.Property)]
    public class InjectAttribute : Attribute
    {

        public From From;
        public bool IncludeInActive;

        public InjectAttribute()
        {
        }

        public InjectAttribute(From from)
        {
            From = from;
        }

        public InjectAttribute(From From, bool IncludeInActive)
        {
            this.From = From;
            this.IncludeInActive = IncludeInActive;
        }

    }

}
