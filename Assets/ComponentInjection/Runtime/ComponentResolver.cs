﻿using System;
using System.Reflection;
using UnityEngine;

namespace ComponentInjection
{

    public static class ComponentResolver
    {

        static BindingFlags BindingFilter = BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic;

        /// <summary>
        /// Finds fields and properties who have a InjectAttribute and automatically resolves their values.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="component"></param>
        public static void ResolveComponents<T>(T component) where T : Component => ResolveBindings(component, component, typeof(T));

        /// <summary>
        /// Finds fields and properties who have a InjectAttribute and automatically resolves their values in to the given resolveTo parameter.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="resolveFrom"></param>
        /// <param name="resolveTo">All resolved fields and properties will be set on this object</param>
        public static void ResolveComponents<T>(Component resolveFrom, T resolveTo) => ResolveBindings(resolveFrom, resolveTo, typeof(T));

        static void ResolveBindings(Component resolveFrom, object resolveTo, Type resolveToType)
        {
            if (resolveFrom == null || resolveTo == null)
            {
                throw new UnityException($"ComponentResolver failed because a null argument was given to the ResolveComponents method.");
            }

            var fields = resolveToType.GetFields(BindingFilter);
            foreach (var field in fields)
            {
                var attrs = field.GetCustomAttributes(typeof(InjectAttribute), false);
                if (attrs.Length > 0)
                {
                    var value = ResolveInjection(resolveFrom, attrs[0] as InjectAttribute, field.FieldType);
                    field.SetValue(resolveTo, value);
                }
            }

            var properties = resolveToType.GetProperties(BindingFilter);
            foreach (var prop in properties)
            {
                var attrs = prop.GetCustomAttributes(typeof(InjectAttribute), false);
                if (attrs.Length > 0)
                {
                    var value = ResolveInjection(resolveFrom, attrs[0] as InjectAttribute, prop.PropertyType);
                    prop.SetValue(resolveTo, value);
                }
            }
        }

        static object ResolveInjection(Component component, InjectAttribute resolve, Type typeToResolve)
        {
            var isArray = typeToResolve.IsArray;
            var choice = (int)resolve.From;

            if (isArray)
            {
                typeToResolve = typeToResolve.GetElementType();
                choice++;
            }

            object value = null;

            switch (choice)
            {
                // self
                case 0:
                    value = component.GetComponent(typeToResolve);
                    break;
                case 1:
                    value = CastArray(component.GetComponents(typeToResolve), typeToResolve);
                    break;

                // parent
                case 2:
                    value = component.GetComponentInParent(typeToResolve);
                    break;
                case 3:
                    value = CastArray(component.GetComponentsInParent(typeToResolve, resolve.IncludeInActive), typeToResolve);
                    break;

                // children
                case 4:
                    value = component.GetComponentInChildren(typeToResolve, resolve.IncludeInActive);
                    break;
                case 5:
                    value = CastArray(component.GetComponentsInChildren(typeToResolve, resolve.IncludeInActive), typeToResolve);
                    break;

                // type
                case 6:
                    value = GameObject.FindObjectOfType(typeToResolve);
                    break;
                case 7:
                    value = CastArray(GameObject.FindObjectsOfType(typeToResolve), typeToResolve);
                    break;
            }

            if (value == null || value.ToString() == "null")
            {
                throw new UnityException($"ComponentResolver failed to resolve the \"{typeToResolve.FullName}\" in the \"{component.GetType().FullName}\" class. This error occurred on the \"{component.name}\" GameObject.");
            }

            return value;
        }

        static Array CastArray(Array from, Type to)
        {
            Array castedArray = Array.CreateInstance(to, from.Length);
            Array.Copy(from, castedArray, from.Length);
            return castedArray;
        }

    }

}