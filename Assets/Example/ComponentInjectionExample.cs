﻿using UnityEngine;
using ComponentInjection;
using static ComponentInjection.ComponentResolver;

public class ComponentInjectionExample : MonoBehaviour
{
    [Inject] BoxCollider selfColliderField;
    
    [Inject] Transform selfColliderProp { get; set; }

    [Inject(From.Children, IncludeInActive = true)] Transform[] childTransformsField;

    [Inject(From.Parent)] Rigidbody parentRigidbodyField;

    [Inject(From.Type)] Camera cameraFromType;

    void Awake() => ResolveComponents(this);

    // Use this for initialization
    void Start()
    {
        Debug.Log(selfColliderField);
        Debug.Log(selfColliderProp);

        Debug.Log(childTransformsField);
        Debug.Log(parentRigidbodyField);

        Debug.Log(cameraFromType);
    }

}
