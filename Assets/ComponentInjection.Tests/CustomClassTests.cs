﻿using ComponentInjection;
using NUnit.Framework;
using UnityEngine;

public class CustomClassTests
{

    GameObject testGameObject;

    [SetUp]
    public void BeforeEach()
    {
        testGameObject = new GameObject("Test Game Object");
        testGameObject.AddComponent<Rigidbody>();
        testGameObject.AddComponent<BoxCollider>();
        testGameObject.AddComponent<Camera>();
        testGameObject.AddComponent<LineRenderer>();
    }

    [Test, Category("ResolveComponents"), Category("CustomClass")]
    public void Resolves_Public_Members_In_Self_To_Custom_Class()
    {
        var component = testGameObject.AddComponent<SelfTests.TestSelfCompoment>();
        var test = new TestSelfCustomClass();

        ComponentResolver.ResolveComponents(component, test);

        Assert.IsNotNull(test.publicField);
        Assert.IsInstanceOf<Rigidbody>(test.publicField);

        Assert.IsNotNull(test.publicProperty);
        Assert.IsInstanceOf<BoxCollider>(test.publicProperty);
    }

    [Test, Category("ResolveComponents"), Category("CustomClass")]
    public void Resolves_Private_Members_In_Self_To_Custom_Class()
    {
        var component = testGameObject.AddComponent<SelfTests.TestSelfCompoment>();
        var test = new TestSelfCustomClass();

        ComponentResolver.ResolveComponents(component, test);

        test.AssertPrivateField();
        test.AssertPrivateProperty();
    }

    #region Test Components

    public class TestSelfCustomClass
    {
        [Inject] public Rigidbody publicField;

        [Inject] public BoxCollider publicProperty { get; set; }

        [Inject] private Camera privateField;

        [Inject] private LineRenderer privateProperty { get; set; }

        public void AssertPrivateField()
        {
            Assert.IsNotNull(privateField);
            Assert.IsInstanceOf<Camera>(privateField);
        }

        public void AssertPrivateProperty()
        {
            Assert.IsNotNull(privateProperty);
            Assert.IsInstanceOf<LineRenderer>(privateProperty);
        }
    }

    #endregion

}

