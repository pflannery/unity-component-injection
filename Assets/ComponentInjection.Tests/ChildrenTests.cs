﻿using ComponentInjection;
using NUnit.Framework;
using UnityEngine;

public class ChildrenTests
{
    GameObject testGameObject;

    [SetUp]
    public void BeforeEach()
    {
        testGameObject = new GameObject("Test Game Object");

        for (int i = 0; i < 5; i++)
        {
            var childGo = new GameObject($"Child {i}");
            childGo.transform.SetParent(testGameObject.transform);
            childGo.AddComponent<BoxCollider>();
            childGo.AddComponent<LineRenderer>();
            childGo.AddComponent<Camera>();
        }

    }

    [Test, Category("ResolveComponents"), Category("Children")]
    public void Resolves_Public_Arrays_In_Children()
    {
        var test = testGameObject.AddComponent<TestChildrenCompoment>();

        ComponentResolver.ResolveComponents(test);

        Assert.IsNotNull(test.publicField);
        Assert.IsInstanceOf<Transform[]>(test.publicField);
        Assert.AreEqual(test.transform.childCount, test.publicField.Length - 1);

        Assert.IsNotNull(test.publicProperty);
        Assert.IsInstanceOf<BoxCollider[]>(test.publicProperty);
        Assert.AreEqual(test.transform.childCount, test.publicProperty.Length);
    }

    [Test, Category("ResolveComponents"), Category("Children")]
    public void Resolves_Private_Arrays_In_Children()
    {
        var test = testGameObject.AddComponent<TestChildrenCompoment>();

        ComponentResolver.ResolveComponents(test);

        test.AssertPrivateField();
        test.AssertPrivateProperty();
    }

    #region Test Components

    public class TestChildrenCompoment : MonoBehaviour
    {

        [Inject(From.Children)] public Transform[] publicField;

        [Inject(From.Children)] public BoxCollider[] publicProperty { get; set; }

        [Inject(From.Children)] private Camera[] privateField;

        [Inject(From.Children)] private LineRenderer[] privateProperty { get; set; }

        public void AssertPrivateField()
        {
            Assert.IsNotNull(privateField);
            Assert.IsInstanceOf<Camera[]>(privateField);
            Assert.AreEqual(privateField.Length, transform.childCount);
        }

        public void AssertPrivateProperty()
        {
            Assert.IsNotNull(privateProperty);
            Assert.IsInstanceOf<LineRenderer[]>(privateProperty);
            Assert.AreEqual(privateProperty.Length, transform.childCount);
        }

    }

    #endregion

}
