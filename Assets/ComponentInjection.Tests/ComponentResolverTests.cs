﻿using ComponentInjection;
using NUnit.Framework;
using UnityEngine;

public class ComponentResolverTests
{

    [Test, Category("ResolveComponents"), Category("ComponentResolver")]
    public void Throws_UnityError_When_ResolveFrom_Is_Null()
    {
        Component testArg = null;

        TestDelegate testDelegate = () => ComponentResolver.ResolveComponents(testArg);

        // Assert
        Assert.That(
            testDelegate,
            Throws.TypeOf<UnityException>()
                .With
                .Message
                .Contains($"ComponentResolver failed because a null argument was given to the ResolveComponents method.")
        );

    }

    [Test, Category("ResolveComponents"), Category("ComponentResolver")]
    public void Throws_UnityError_When_ResolveTo_Is_Null()
    {
        var testGameObject = new SelfTests.TestSelfErrorComponent();
        Component testArg = null;

        TestDelegate testDelegate = () => ComponentResolver.ResolveComponents(testGameObject, testArg);

        // Assert
        Assert.That(
            testDelegate,
            Throws.TypeOf<UnityException>()
                .With
                .Message
                .Contains($"ComponentResolver failed because a null argument was given to the ResolveComponents method.")
        );

    }


}

