﻿using ComponentInjection;
using NUnit.Framework;
using UnityEngine;

public class ParentTests
{

    GameObject testGameObject;

    [SetUp]
    public void BeforeEach()
    {
        var parentGameObject = new GameObject("Test Parent");
        parentGameObject.AddComponent<Rigidbody>();
        parentGameObject.AddComponent<BoxCollider>();
        parentGameObject.AddComponent<Camera>();
        parentGameObject.AddComponent<LineRenderer>();

        testGameObject = new GameObject("Test GameObject");
        testGameObject.transform.SetParent(parentGameObject.transform);
    }

    [Test, Category("ResolveComponents"), Category("Parent")]
    public void Resolves_Public_Members_In_Parent()
    {
        var test = testGameObject.AddComponent<TestParentCompoment>();

        ComponentResolver.ResolveComponents(test);

        Assert.IsNotNull(test.publicField);
        Assert.IsInstanceOf<Rigidbody>(test.publicField);
        Assert.AreEqual("Test Parent", test.publicField.gameObject.name);

        Assert.IsNotNull(test.publicProperty);
        Assert.IsInstanceOf<BoxCollider>(test.publicProperty);
        Assert.AreEqual("Test Parent", test.publicProperty.gameObject.name);
    }

    [Test, Category("ResolveComponents"), Category("Parent")]
    public void Resolves_Private_Members_In_Parent()
    {
        var test = testGameObject.AddComponent<TestParentCompoment>();

        ComponentResolver.ResolveComponents(test);

        test.AssertPrivateField();
        test.AssertPrivateProperty();
    }

    #region Test Components

    public class TestParentCompoment : MonoBehaviour
    {
        [Inject(From.Parent)] public Rigidbody publicField;

        [Inject(From.Parent)] public BoxCollider publicProperty { get; set; }

        [Inject(From.Parent)] private Camera privateField;

        [Inject(From.Parent)] private LineRenderer privateProperty { get; set; }

        public void AssertPrivateField()
        {
            Assert.IsNotNull(privateField);
            Assert.IsInstanceOf<Camera>(privateField);
            Assert.AreEqual("Test Parent", privateField.gameObject.name);
        }

        public void AssertPrivateProperty()
        {
            Assert.IsNotNull(privateProperty);
            Assert.IsInstanceOf<LineRenderer>(privateProperty);
            Assert.AreEqual("Test Parent", privateField.gameObject.name);
        }
    }

    #endregion

}

