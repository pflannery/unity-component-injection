﻿using ComponentInjection;
using NUnit.Framework;
using UnityEngine;

public class SelfTests
{

    GameObject testGameObject;

    [SetUp]
    public void BeforeEach()
    {
        testGameObject = new GameObject("Test Game Object");
        testGameObject.AddComponent<Rigidbody>();
        testGameObject.AddComponent<BoxCollider>();
        testGameObject.AddComponent<Camera>();
        testGameObject.AddComponent<LineRenderer>();
    }

    [Test, Category("ResolveComponents"), Category("Self")]
    public void Resolves_Public_Members()
    {
        var test = testGameObject.AddComponent<TestSelfCompoment>();

        ComponentResolver.ResolveComponents(test);

        Assert.IsNotNull(test.publicField);
        Assert.IsInstanceOf<Rigidbody>(test.publicField);

        Assert.IsNotNull(test.publicProperty);
        Assert.IsInstanceOf<BoxCollider>(test.publicProperty);
    }

    [Test, Category("ResolveComponents"), Category("Self")]
    public void Resolves_Private_Members()
    {
        var test = testGameObject.AddComponent<TestSelfCompoment>();

        ComponentResolver.ResolveComponents(test);

        test.AssertPrivateField();
        test.AssertPrivateProperty();
    }

    [Test, Category("ResolveComponents"), Category("Self")]
    public void Throws_UnityError_For_Unresolved_Members()
    {
        var test = testGameObject.AddComponent<TestSelfErrorComponent>();

        TestDelegate testDelegate = () => ComponentResolver.ResolveComponents(test);

        // Assert
        Assert.That(
            testDelegate,
            Throws.TypeOf<UnityException>()
                .With
                .Message
                .Contains($"ComponentResolver failed to resolve the \"{typeof(SpriteRenderer).FullName}\" in the \"{typeof(TestSelfErrorComponent).FullName}\" class. This error occurred on the \"{testGameObject.name}\" GameObject.")
        );
    }


    #region Test Components

    public class TestSelfCompoment : MonoBehaviour
    {
        [Inject] public Rigidbody publicField;

        [Inject] public BoxCollider publicProperty { get; set; }

        [Inject] private Camera privateField;

        [Inject] private LineRenderer privateProperty { get; set; }

        public void AssertPrivateField()
        {
            Assert.IsNotNull(privateField);
            Assert.IsInstanceOf<Camera>(privateField);
        }

        public void AssertPrivateProperty()
        {
            Assert.IsNotNull(privateProperty);
            Assert.IsInstanceOf<LineRenderer>(privateProperty);
        }
    }

    public class TestSelfErrorComponent : MonoBehaviour
    {
        [Inject] public SpriteRenderer errorField;
    }

    #endregion

}
